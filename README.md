# OpenNBT for WorldSynth
This is a fork of **[OpenNBT](https://github.com/Steveice10/OpenNBT)** by Steveice10 over on github with modification by the WorldSynth team for use with WorldSynth.

# OpenNBT
OpenNBT is a library for reading and writing NBT files and stringified NBT, with some extra custom tags added to allow the storage of more data types.

## Building the Source
OpenNBT for WorldSynth uses Gradle to manage building and dependencies.

To build OpenNBT, simply run `./gradlew build` in the source's directory.  
To setup for development in Eclipse, simply run `./gradlew eclipse` and import as existing eclipse project.

## License
OpenNBT is licensed under the **[MIT license](http://www.opensource.org/licenses/mit-license.html)**.
